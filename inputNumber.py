from tkinter import *

# Champ input number only
class InputNumber(Entry):
    def __init__(self, master=None, **kwargs):
        self.var = StringVar()
        Entry.__init__(self, master, textvariable=self.var, **kwargs)
        self.old_value = ''
        self.var.trace('w', self.check)
        self.get, self.set = self.var.get, self.var.set

    def check(self, *args):
        if self.get().isdigit() and int(self.get()) <= 10 or not self.get(): 
            self.old_value = self.get()
        else:
            self.set(self.old_value)