from subprocess import run
from minizinc import Instance, Model, Solver
from tkinter import *
from scrollFrame import ScrollableFrame
from inputNumber import InputNumber

def runSolver():
    # Check erreur si une manche est supérieur à 10
    for i in range(0, 20, 2):
        if (lancerArray[i].get() and lancerArray[i+1].get()) and (int(lancerArray[i].get()) + int(lancerArray[i+1].get())) > 10:
            errorLabel['text'] = "Erreur sur la manche " + str(i+1) + " : le score ne peut pas dépasser 10"
            errorLabel.pack()
            return

    lancerTab = [0] * 21
    for i in range(20):
        if lancerArray[i].get():
            lancerTab[i] = int(lancerArray[i].get())

    errorLabel.pack_forget()
    # Load model file
    model = Model("./bowling_finale.mzn")
    # Set the solver
    gecode = Solver.lookup("gecode")
    # Création de l'instance
    instance = Instance(gecode, model)
    # Changement des variables
    instance["score_final"] = score.get()
    if(lancerTab.count(0) != 21):
        instance["lancers"] = lancerTab
    # Lancement de la résolution
    resultat = instance.solve(nr_solutions=int(inputNumber.get()))
    printResult([resultat])

# Fonction d'affichage des résultats
def printResult(result):
    for widget in resultFrame.winfo_children():
       widget.destroy()

    timeLabel = Label(resultFrame, text="Temps de résolution : " + str(result[0].statistics['solveTime'].total_seconds()*1000) + " ms")
    timeLabel.pack(fill="both", expand="yes")

    numberLabel = Label(resultFrame, text="Nombre de solutions : " + str(result[0].statistics['solutions']))
    numberLabel.pack(fill="both", expand="yes")

    scrollFrame = ScrollableFrame(resultFrame)
    scrollFrame.pack(fill="both", expand="yes")

    for i in range(len(result[0])):
        printArrayResult(result[0][i], i+1, scrollFrame.scrollable_frame)

def printArrayResult(result, i, scrollFrame):
    scoreList = result.lancers

    frame = LabelFrame(scrollFrame, text="#" + str(i))
    frame.pack(side=TOP, pady=5,padx=30)
    # Ligne Manche
    mancheLabel = Label(frame, text="Manche", relief="ridge",width=15, bg='#fff')
    mancheLabel.grid(row=0, column=0)

    for i in range(10):
        a = Label(frame, text=i+1,relief="ridge",width=4)
        if i == 9:
            a.grid(row=0,column=i*2+1,columnspan=3,sticky="nsew")
        else:
            a.grid(row=0,column=i*2+1,columnspan=2,sticky="nsew")

    # Ligne Lancer
    lancerLabel = Label(frame, text="Lancer", relief="ridge",width=15, bg='#fff')
    lancerLabel.grid(row=1, column=0)

    for i in range(21):
        b = Label(frame, text=i+1,relief="ridge",width=4)
        b.grid(row=1, column=i+1,sticky="nsew")

    # Ligne score
    quilleLabel = Label(frame, text="Quilles tombés", relief="ridge",width=15, bg='#fff')
    quilleLabel.grid(row=2, column=0)

    for i in range(21):
        c = Label(frame,text=scoreList[i], width=4, relief="ridge",bg='#fff')
        c.grid(row=2, column=i+1,sticky="nsew")

    # Ligne strike
    strikeLabel = Label(frame, text="Strike", relief="ridge",width=15, bg='#fff')
    strikeLabel.grid(row=3,column=0)
    for i in range(12):
        if i < 9:
            a = Label(frame,relief="ridge",width=4)
            if scoreList[i*2] == 10 : a.config(bg="#39DF1B")
            a.grid(row=3,column=i*2+1,columnspan=2,sticky="nsew")
        else:
            a = Label(frame,relief="ridge",width=4)
            if scoreList[9+i] == 10 : a.config(bg="#39DF1B")
            a.grid(row=3,column=10+i,sticky="nsew")

    # Ligne spare
    spareLabel = Label(frame, text="Spare", relief="ridge",width=15, bg='#fff')
    spareLabel.grid(row=4,column=0)
    for i in range(0,20,2):
        a = Label(frame,relief="ridge",width=4)
        if scoreList[i] != 10 and scoreList[i] + scoreList[i+1] == 10 : a.config(bg="#FFD700")
        a.grid(row=4,column=i+1,columnspan=3,sticky="nsew")
        b = Label(frame, relief="ridge", width=4, bg='#C0C0C0')
        b.grid(row=4, column=21)

    # Ligne Gouttière
    gouttiereLabel = Label(frame, text="Gouttière", relief="ridge",width=15, bg='#fff')
    gouttiereLabel.grid(row=5,column=0)
    for i in range(21):
        a = Label(frame,relief="ridge",width=4)
        if scoreList[i-1] != 10 and scoreList[i] == 0 : a.config(bg="#eb4034")
        a.grid(row=5,column=i+1,sticky="nsew")

# Création fenêtre
window = Tk()
window.title("Projet Bowling")

# Paramètre de l'instance
runFrame = LabelFrame(window,text="Paramètres",padx=10,pady=10)
runFrame.pack(fill="both", expand="yes")

score = Scale(runFrame, from_=0, to=300, orient=HORIZONTAL, label="Objectif de score", troughcolor="grey")
score.set(150)
score.pack(fill="x", expand="yes")

#Tableau des lancers
scoreTab = LabelFrame(runFrame, text="Lancer")
scoreTab.pack(fill="x", expand="yes")

# Ligne Manche
mancheLabel = Label(scoreTab, text="Manche", relief="ridge",width=15, bg='#fff')
mancheLabel.grid(row=0, column=0)

for i in range(10):
    a = Label(scoreTab, text=i+1,relief="ridge",width=5)
    a.grid(row=0,column=i*2+1,columnspan=2,sticky="nsew")

# Ligne Lancer
lancerLabel = Label(scoreTab, text="Lancer", relief="ridge",width=15, bg='#fff')
lancerLabel.grid(row=1, column=0)

for i in range(20):
    b = Label(scoreTab, text=i+1,relief="ridge",width=5)
    b.grid(row=1, column=i+1,sticky="nsew")

# Ligne score
quilleLabel = Label(scoreTab, text="Quilles tombés", relief="ridge",width=15, bg='#fff')
quilleLabel.grid(row=2, column=0)

lancerArray = []
for i in range(20):
    c = InputNumber(scoreTab,width=5, bg='#fff',justify='center')
    c.grid(row=2, column=i+1,sticky="nsew")
    lancerArray.append(c)

# Check pour choisir d'afficher tous les résultats
choiceNumberLabel = Label(runFrame, text="Nombre de solutions à afficher")
choiceNumberLabel.pack()
inputNumber = InputNumber(runFrame, width=5,bg='#fff',justify='center')
inputNumber.set("1")
inputNumber.pack()

# Bouton qui permet de lancer le solver
runBT = Button(runFrame, text="CALCULER", command=runSolver,bg='#fff')
runBT.pack(fill="x", expand="yes")

# Label d'affichage des erreurs
errorLabel = Label(runFrame,fg="red",justify='center')
errorLabel.pack_forget()

# Frame des résultats
resultFrame = LabelFrame(window,text="Résultat",padx=10,pady=10)
resultFrame.pack(fill="both", expand="yes")

window.mainloop()